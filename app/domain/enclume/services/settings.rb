# frozen_string_literal: true

module Domain
  module Enclume
    module Services
      class Settings
        def call
          settings = Forgeron::App[:settings].to_h
          settings[:tty][:output] = settings[:tty][:output].inspect

          puts settings.to_yaml
        end
      end
    end
  end
end
