# frozen_string_literal: true

Forgeron::App.require_from_root("lib/version")

module Domain
  module Enclume
    module Services
      class Version
        def call
          puts "Version: #{Forgeron::VERSION}"
        end
      end
    end
  end
end
