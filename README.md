<!-- PROJECT SHIELDS -->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/akarzim/forgeron">
    <img src="public/images/forgeron.png" alt="Logo" width="445" height="125">
  </a>

  <h3 align="center">Forgeron</h3>

  <p align="center">
    CLI tools to manage GitLab & GitHub based projects.
    <br />
    <a href="https://gitlab.com/akarzim/forgeron"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/akarzim/forgeron/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/akarzim/forgeron/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

## About The Project

**DISCLAIMER**: This is a toy project to play with [dry-types][dry-types],
[dry-system][dry-system] & [dry-cli][dry-cli] Ruby gems.

### Why this name?

Forgeron is the french word for blacksmith and GitLab & GitHub are software forges.

### Built With

* [dry-cli][dry-cli]
* [dry-system][dry-system]
* [dry-types][dry-types]
* [tty-command][tty-command]

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* Credentials to [Gitlab](https://www.gitlab.com/) API, or
* Credentials to [GitHub](https://www.github.com/) API

### Installation

1. Clone the repo

```sh
git clone https://gitlab.com/akarzim/forgeron.git
```

## Usage

In a terminal, step inside the Forgeron directory and run:

```sh
❯ ./forgeron --help
Commands:
  forgeron console           # Launch a console
  forgeron settings          # Print settings
  forgeron version           # Print version
```

## Roadmap

See the [open issues](https://gitlab.com/akarzim/forgeron/issues) for a list of
proposed features (and known issues).

## Contributing

Contributions are what make the open source community such an amazing place to
be learn, inspire, and create. Any contributions you make are **greatly
appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

François Vantomme - [@akarzim](https://mastodon.host/@akarzim)

Project Link: <https://gitlab.com/akarzim/forgeron>

## Acknowledgements

* [Othneil Drew][othneildrew] for [this readme template][readme-template]

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/gitlab/contributors/akarzim/forgeron.svg?style=flat-square
[contributors-url]: https://gitlab.com/akarzim/forgeron/-/project_members
[forks-shield]: https://img.shields.io/gitlab/forks/akarzim/forgeron.svg?style=flat-square
[forks-url]: https://gitlab.com/akarzim/forgeron/-/forks
[stars-shield]: https://img.shields.io/gitlab/stars/akarzim/forgeron.svg?style=flat-square
[stars-url]: https://gitlab.com/akarzim/forgeron/-/starrers
[issues-shield]: https://img.shields.io/gitlab/issues/open/akarzim/forgeron.svg?style=flat-square
[issues-url]: https://gitlab.com/akarzim/forgeron/-/issues
[license-shield]: https://img.shields.io/gitlab/license/akarzim/forgeron.svg?style=flat-square
[license-url]: https://gitlab.com/akarzim/forgeron/-/blob/main/LICENSE.txt
[dry-cli]: https://dry-rb.org/gems/dry-cli/main/
[dry-system]: https://dry-rb.org/gems/dry-system/main/
[dry-types]: https://dry-rb.org/gems/dry-types/main/
[tty-command]: https://gitlab.com/piotrmurach/tty-command
[othneildrew]: https://gitlab.com/othneildrew
[readme-template]: https://gitlab.com/othneildrew/Best-README-Template
