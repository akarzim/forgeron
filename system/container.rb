# frozen_string_literal: true

require "logger"
require "pastel"
require "tty-command"
require "dry/system"

module Forgeron
  class App < Dry::System::Container
    use :env, inferrer: -> { ENV.fetch("APP_ENV", :development).to_sym }
    use :logging

    configure do |config|
      config.component_dirs.add "system"

      config.component_dirs.add "app" do |dir|
        dir.namespaces.add "domain", key: nil
      end

      config.logger = Logger.new("forgeron.log")
    end

    register(:color) { Pastel.new }
    register(:tty) { ::TTY::Command.new(**tty_settings) }

    def self.tty_settings
      Forgeron::App[:settings]
        .to_h
        .keep_if { |k, _| k.start_with?("tty_") }
        .to_h { |k, v| [k.to_s.delete_prefix("tty_").to_sym, v.call] }
    end
    private_class_method :tty_settings
  end
end
