# frozen_string_literal: true

require "yaml"
require_relative "config"

module Forgeron
  class ConfigLoader
    FALLBACK_CONFIG = {}.freeze

    def initialize(**kwargs)
      attrs = kwargs.transform_keys(&:to_sym)
      @default_config_file = attrs.fetch(:default_config_file, ".forgeron.yml")
      @override_config_file = attrs.fetch(:override_config_file, ".forgeron.override.yml")
    end

    def call
      default_config.deep_merge(override_config)
    end

    private

    def load_config(filepath)
      config = Forgeron::App.root.join(filepath)
      File.file?(config) ? Psych.load_file(config, fallback: FALLBACK_CONFIG) : FALLBACK_CONFIG
    end

    def default_config
      @default_config ||= Forgeron::Config.new(**load_config(@default_config_file))
    end

    def override_config
      @override_config ||= Forgeron::Config.new(**load_config(@override_config_file))
    end
  end
end
