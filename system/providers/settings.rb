# frozen_string_literal: true

require "dry/system"
require "dry/system/provider_sources"

Forgeron::App.register_provider(:settings, from: :dry_system) do # rubocop:disable Metrics/BlockLength
  before :prepare do
    require "types"
    require "config_loader"

    register(:defaults, Forgeron::ConfigLoader.new.call)
  end

  settings do
    setting :forgeron do
      setting :gitlab_api, constructor: Types::String, default: Forgeron::App[:defaults].forgeron.gitlab_api.freeze
      setting :github_api, constructor: Types::String, default: Forgeron::App[:defaults].forgeron.github_api.freeze
    end

    setting :tty do
      setting :output, constructor: Types.Interface(:<<), default: $stdout

      setting :color, constructor: Types::Bool, default: Forgeron::App[:defaults].tty.color
      setting :uuid, constructor: Types::Bool, default: Forgeron::App[:defaults].tty.uuid
      setting :dry_run, constructor: Types::Bool, default: Forgeron::App[:defaults].tty.dry_run

      setting :printer,
              constructor: Types::Coercible::Symbol.enum(:null, :pretty, :progress, :quiet),
              default: Forgeron::App[:defaults].tty.printer

      setting :verbose, constructor: Types::Bool, default: Forgeron::App[:defaults].tty.verbose
      setting :pty, constructor: Types::Bool, default: Forgeron::App[:defaults].tty.pty
      setting :binmode, constructor: Types::Bool, default: Forgeron::App[:defaults].tty.binmode
      setting :timeout, constructor: Types::Integer.optional, default: Forgeron::App[:defaults].tty.timeout

      setting :only_output_on_error,
              constructor: Types::Bool,
              default: Forgeron::App[:defaults].tty.only_output_on_error
    end
  end
end
