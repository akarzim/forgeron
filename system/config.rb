# frozen_string_literal: true

require "dry/struct"

module Forgeron
  class Config < Dry::Struct
    transform_keys(&:to_sym)

    attribute? :forgeron do
      attribute :gitlab_api, Types::String.default("https://gitlab.com/api/v4")
      attribute :github_api, Types::String.default("https://api.github.com/")
    end

    attribute? :tty do
      attribute :color, Types::Bool.default(true)
      attribute :uuid, Types::Bool.default(true)
      attribute :printer, Types::Coercible::Symbol.default(:pretty).enum(:null, :pretty, :progress, :quiet)
      attribute :dry_run, Types::Bool.default(false)
      attribute :verbose, Types::Bool.default(true)
      attribute :pty, Types::Bool.default(false)
      attribute :binmode, Types::Bool.default(false)
      attribute? :timeout, Types::Integer.optional
      attribute :only_output_on_error, Types::Bool.default(false)
    end

    def self.deep_merge(origin, other, &block)
      this_hash = origin.to_h
      other_hash = other.to_h

      merged_hash = this_hash.merge(other_hash) do |key, this_val, other_val|
        if this_val.is_a?(Hash) && other_val.is_a?(Hash)
          deep_merge(this_val, other_val, &block)
        elsif block_given?
          block.call(key, this_val, other_val)
        else
          other_val
        end
      end

      new(**merged_hash)
    end

    def deep_merge(other)
      self.class.deep_merge(self, other)
    end
  end
end
