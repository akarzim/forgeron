# frozen_string_literal: true

require "pry"

module Forgeron
  module CLI
    module Commands
      class Console < Dry::CLI::Command
        desc "Launch a console"

        def call
          Pry.start
        end
      end
    end
  end
end
