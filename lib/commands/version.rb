# frozen_string_literal: true

require "deps"

module Forgeron
  module CLI
    module Commands
      class Version < Dry::CLI::Command
        include Deps["enclume.services.version"]

        desc "Print version"

        def call
          version.call
        end
      end
    end
  end
end
