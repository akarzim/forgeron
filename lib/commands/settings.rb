# frozen_string_literal: true

require "deps"

module Forgeron
  module CLI
    module Commands
      class Settings < Dry::CLI::Command
        include Deps["enclume.services.settings"]

        desc "Print settings"

        def call
          settings.call
        end
      end
    end
  end
end
