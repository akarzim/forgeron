# frozen_string_literal: true

Forgeron::App.require_from_root("lib/commands/*")
